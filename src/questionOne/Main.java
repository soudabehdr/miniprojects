package questionOne;

import questionFour.FilterImage;
import questionThree.LCS;
import questionTwo.CharDictionary;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Soudabeh on 3/17/2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        Main main= new Main();
        main.runQuestionOne();
        main.runQuestionTwo();
        main.runQuestionThree();
        main.runQuestionFour();




    }

    public void runQuestionOne()throws IOException{
        ArrayList scannerArray= new ArrayList<Integer>();
        SumSquares sumSquares= new SumSquares();
        Scanner scanner= new Scanner(System.in);
        System.out.println("m ra vared konid:");
        int number= scanner.nextInt();
        System.out.println("be tedade m adad vared konid:");
        for(int i=0 ; i<number ; i++){
            scannerArray.add(scanner.nextInt());
        }
        sumSquares.setter(scannerArray);
        sumSquares.writeOutSquare();
    }

//----------------------------------------------------------------------------------------------------------------------

    public void runQuestionTwo() throws IOException{
        CharDictionary charDictionary= new CharDictionary();
        HashMap map = charDictionary.charFrequency("e:\\sodab.txt");
        charDictionary.displayfrequency(map);
        charDictionary.saveFrequency(map);
    }

//----------------------------------------------------------------------------------------------------------------------



    public void runQuestionThree()throws IOException{

        LCS lcs= new LCS();
        Scanner scanner = new Scanner(System.in);
        System.out.println("please Enter first String:");
        String input1= scanner.nextLine();
        System.out.println("please Enter second String:");
        String input2=scanner.nextLine();
        File fileIn1=new File("d:\\file1.txt");
        File fileIn2=new File("d:\\file2.txt");
        FileWriter fileWriterIn1=new FileWriter(fileIn1);
        FileWriter fileWriterIn2=new FileWriter(fileIn2);
        fileWriterIn1.write(input1);
        fileWriterIn2.write(input2);
        fileWriterIn1.flush();
        fileWriterIn1.close();
        fileWriterIn2.flush();
        fileWriterIn2.close();
        System.out.println("longest common subString is:"); // inam baraye chap kardan injast k nmidonm mikhad ya na
        lcs.longestCommonSubsequence("d:\\file1.txt","d:\\file2.txt");
    }
//----------------------------------------------------------------------------------------------------------------------
    public void runQuestionFour() throws IOException{

        FilterImage filterImage= new FilterImage();
         filterImage.filterBlueColor("e:\\pix.jpg");
         filterImage.filterGreenColor("e:\\pix.jpg");
         filterImage.filterRedColor("e:\\pix.jpg");
         filterImage.blackWhite("e:\\pix.jpg");
         filterImage.randomImage();
         filterImage.duplicateImage("e:\\pix.jpg");
         filterImage.negativeImage("e:\\pix.jpg");

    }
}

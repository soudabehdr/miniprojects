package questionOne;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Soudabeh on 3/17/2017.
 */
public class SumSquares {


    /**
     * arrayList yek arrayList az noe Integer ijad mikonad.
     */
    ArrayList<Integer> arrayList = new ArrayList<Integer>();


    /**
     * in tabe yek arrayeList ra gerefte va rooye motaghayer class set mikonad
     * @param arrayList vorodie tabe va az noe Integer ast
     * @throws IOException Exeption az noe IO ke be tabe asli midahim k handle koand
     */

    public void setter (ArrayList<Integer> arrayList) throws IOException{

        this.arrayList=arrayList;

    }


    /**
     * writeOutSquare yek method k vorodi nadarad va adad ra b tavane 2 resande
     * va dar nahayat majmoe morabaat ra mohasebe mokonad.
     * @throws IOException Exeption az noe IO ke be tabe asli midahim k handle koand
     */
    public void writeOutSquare() throws IOException{

        File fileSum= new File("e:\\sumSquare.txt");
        fileSum.createNewFile();
        FileWriter fileWriter= new FileWriter(fileSum);
        BufferedWriter bufferedWriter= new BufferedWriter(fileWriter);
        int sum=0;

        for (int i = 0; i < arrayList.size(); i++)
        {
            int result = (int)Math.pow(arrayList.get(i),2);
            String resultToString = Integer.toString(result);
            bufferedWriter.write(resultToString+" ");
            sum = sum+result;
        }
        bufferedWriter.write("//first line of your output file");
        bufferedWriter.newLine();
        String sumToString=Integer.toString(sum);
        bufferedWriter.write(sumToString);
        bufferedWriter.write("//second line of your output file");
        bufferedWriter.flush();
        bufferedWriter.close();
    }



}

package questionTwo;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Soudabeh on 3/19/2017.
 */
public class CharDictionary {

    /**
     * charFrequency address yek file ra b onvane vorodi gerefte va tedad character haye estefade shode
     * ra dar hashMap zakhire mikonad.
     * @param address vorodi az noe String k address file ra migirad.
     * @return HashMap ra k tedad characterha dar aan zakhire shode ast ra bar migardanad.
     * @throws IOException Exeption az noe IO ke be tabe asli midahim k handle koand
     */
    public HashMap charFrequency(String address) throws IOException{
        File file = new File(address);
        FileReader fileReader= new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String str1 =bufferedReader.readLine().toLowerCase();
        char [] str1toChar = str1.toCharArray();
        Arrays.sort(str1toChar);
        String sortedStr1 = new String(str1toChar);
        HashMap<Character,Integer> map = new HashMap<Character,Integer>();

        for(int i = 0; i < sortedStr1.length(); i++){
            char c = sortedStr1.charAt(i);
            Integer value = map.get(new Character(c));
            if(value != null){
                map.put(c, new Integer(value+1));
            }else{
                map.put(c,1);
            }

        }
        return map;


    }

    /**
     * displayfrequency tedad character ha ra b tartibe horofe alefba neshan midahad.
     *
     * @param hashMap vorodie tabe ast ke hashMap tolid shode ra migirad va b tartibe horofe alefba sort mikonad
     */
    public void displayfrequency(HashMap hashMap) {

        for(int j=91; j<127;j++){
            char b = (char) j;
            if(hashMap.get(b)!=null){
                System.out.print(b+"=");
                System.out.println(hashMap.get(b));
                System.out.println("\n");
            }

        }

        for (int i = 33; i < 65; i++) {
            char a = (char) i;
            if (hashMap.get(a) != null) {
                System.out.print(a + "=");
                System.out.println(hashMap.get(a));
                System.out.println("\n");
            }
        }


    }


    /**
     * saveFrequency horofe va tedade character ha ra dar yek file be name frequency.txt minevisad
     * @param hashMap vorodie tabe ke hashMap tolid shode ra migirad va dar file minevisad
     * @throws IOException Exeption az noe IO ke be tabe asli midahim k handle koand
     */
    public void saveFrequency(HashMap hashMap) throws IOException{
        File file= new File("e:\\frequency.txt");
        file.createNewFile();
        FileWriter fileWriter= new FileWriter(file);
        BufferedWriter bufferedWriter= new BufferedWriter(fileWriter);
        for(char i='a' ; i<'z';i++){
            if(hashMap.get(i)!= null)
            {
                bufferedWriter.write(i+"=");
                bufferedWriter.write(hashMap.get(i).toString());
                bufferedWriter.newLine();
            }
        }
        if(hashMap.get('.')!=null){
            bufferedWriter.write(".=");
            bufferedWriter.write(hashMap.get('.').toString());

        }
        bufferedWriter.flush();
        bufferedWriter.close();


    }




    }


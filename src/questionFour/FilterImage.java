package questionFour;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


import static com.sun.javafx.iio.ios.IosImageLoader.RGB;

/**
 * Created by Soudabeh on 3/19/2017.
 */
public class FilterImage {


    public FilterImage(String inputAdreess , String outputAddress){

    }

    public FilterImage(){

    }

    public void filterRedColor(String address) throws IOException{

        File file =new File(address);
        BufferedImage bufferedImage=  ImageIO.read(file);
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                int alpha = new Color(bufferedImage.getRGB(y, x)).getAlpha();
                int red = 0;
                int green = new Color(bufferedImage.getRGB(y, x)).getGreen();
                int blue =  new Color(bufferedImage.getRGB(y, x)).getBlue();
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;

                bufferedImage.setRGB(y, x, pixel);
            }
        try {
            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }

    }

    public void filterGreenColor(String address) throws IOException{
        File file= new File(address);
        BufferedImage bufferedImage=  ImageIO.read(file);
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                int alpha = new Color(bufferedImage.getRGB(y, x)).getAlpha();
                int red = new Color(bufferedImage.getRGB(y, x)).getRed();
                int green = 0;
                int blue =  new Color(bufferedImage.getRGB(y, x)).getBlue();
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;

                bufferedImage.setRGB(y, x, pixel);
            }
        try {
            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }


    }
    public void filterBlueColor(String address) throws IOException{
        File file= new File(address);
        BufferedImage bufferedImage=  ImageIO.read(file);
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                int alpha = new Color(bufferedImage.getRGB(y, x)).getAlpha();
                int red = new Color(bufferedImage.getRGB(y, x)).getRed();
                int green =  new Color(bufferedImage.getRGB(y, x)).getGreen();
                int blue =  0;
                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;

                bufferedImage.setRGB(y, x, pixel);
            }
        try {
            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }

    }


//----------------------------------------------------------------------------------------------------------------------


    public void blackWhite(String address) throws IOException{

        File file= new File(address);
        BufferedImage bufferedImage=  ImageIO.read(file);

        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                int alpha = new Color(bufferedImage.getRGB(y, x)).getAlpha();
                int r = new Color(bufferedImage.getRGB(y, x)).getRed();
                int g =  new Color(bufferedImage.getRGB(y, x)).getGreen();
                int b =  new Color(bufferedImage.getRGB(y, x)).getBlue();
                int avg = (r + g + b) / 3;

                int red = avg;
                int green = avg;
                int blue = avg;

                int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;

                bufferedImage.setRGB(y, x, pixel);
            }
        try {
            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }

    }


    public void randomImage() throws IOException{

        File file= new File("e:\\randomImage.jpg");
        file.createNewFile();
        int width=200;  int height=200;
        BufferedImage bufferedImage= new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int alpha = (int)(Math.random()*256);
                int red = (int)(Math.random()*256);
                int green = (int)(Math.random()*256);
                int blue = (int)(Math.random()*256);

                int pixel = (alpha<<24) | (red<<16) | (green<<8) | blue;

                bufferedImage.setRGB(x, y, pixel);
            }
        }

       try {
           ImageIO.write(bufferedImage,"jpg",file);
       }catch (IOException e){
           System.out.println("error:" +e);
       }

    }


    public void duplicateImage(String picAddress) throws IOException {

            File file= new File(picAddress);
            BufferedImage bufferedImage= ImageIO.read(file);
            BufferedImage bufferedImage1=new BufferedImage(bufferedImage.getWidth(),
                    bufferedImage.getHeight()*2,BufferedImage.TYPE_INT_ARGB);

            for (int y = 0; y < bufferedImage.getHeight(); y++)
                for (int x = 0; x < bufferedImage.getWidth(); x++) {

                    int alpha = new Color(bufferedImage.getRGB(y, x)).getAlpha();
                    int red = new Color(bufferedImage.getRGB(y, x)).getRed();
                    int green =  new Color(bufferedImage.getRGB(y, x)).getGreen();
                    int blue =  new Color(bufferedImage.getRGB(y, x)).getBlue();
                    int pixel = (alpha << 24) | (red << 16) | (green << 8) | blue;
                    bufferedImage1.setRGB(y , x, pixel);
                    bufferedImage1.setRGB(y , x+bufferedImage.getHeight(), pixel);
                }
            try {

                ImageIO.write(bufferedImage1,"jpg",file);
            }catch (IOException e){
                System.out.println("error:" +e);
            }
        }


    public void negativeImage(String address) throws IOException {

        int sum = 0;
        Float[][] mask = new Float[][]{
                {0.25f, 0.25f, 0.25f},
                {0.25f, 0.25f, 0.25f},
                {0.25f, 0.25f, 0.25f}
        };

        File file = new File(address);
        BufferedImage bufferedImage = ImageIO.read(file);

        for (int y = 0; y < bufferedImage.getHeight(); y++) {
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                if (y == 0 && x != 0 && x != bufferedImage.getWidth() - 1) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = -1; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/6;
                    bufferedImage.setRGB(y,x,sum);
                }


                if (y == bufferedImage.getHeight() - 1 && x != 0 && x != bufferedImage.getWidth() - 1) {
                    for (int i = -1; i < 1; i++) {
                        for (int j = -1; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/6;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (x == 0 && y != 0 && y != bufferedImage.getHeight() - 1) {
                    for (int i = -1; i < 2; i++) {
                        for (int j = 0; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/6;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (x == bufferedImage.getWidth() - 1 && y != 0 && y != bufferedImage.getHeight() - 1) {
                    for (int i = -1; i < 2; i++) {
                        for (int j = -1; j < 1; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/6;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (x == 0 && y == 0 ) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/4;
                    bufferedImage.setRGB(y,x,sum);
                }
                if ( y == 0 && x == bufferedImage.getWidth()-1 ) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = -1; j < 1; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/4;
                    bufferedImage.setRGB(y,x,sum);
                }

                if ( y == bufferedImage.getHeight()-1 && x == 0) {
                    for (int i = -1; i < 1; i++) {
                        for (int j = 0; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/4;
                    bufferedImage.setRGB(y,x,sum);
                }

                if ( y == bufferedImage.getHeight()-1 && x == bufferedImage.getWidth()-1) {
                    for (int i = -1; i < 1; i++) {
                        for (int j = -1; j < 1; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/4;
                    bufferedImage.setRGB(y,x,sum);
                }


                if (x != 0 && x != bufferedImage.getWidth() - 1 && y != 0 && y != bufferedImage.getHeight() - 1) {

                    for (int i = -1; i < 2; i++) {
                        for (int j = -1; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/9;
                    bufferedImage.setRGB(y,x,sum);
                }
            }


        }

        try {

            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }
    }

//----------------------------------------------------------------------------------------------------------------------
    public void motionBlurImage(String address) throws IOException {

        int sum = 0;
        Integer[][] mask = new Integer[][]{
                {1, 0, 0,0,0},
                {0, 1, 0,0,0},
                {0,0,1,0,0},
                {0,0,0,1,0},
                {0,0,0,0,1}
        };

        File file = new File(address);
        BufferedImage bufferedImage = ImageIO.read(file);

        for (int y = 0; y < bufferedImage.getHeight(); y++) {
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                if (y == 0 && x != 0 && x!=1 && x != bufferedImage.getWidth() - 1 &&x!=bufferedImage.getWidth()-2) {
                    for (int i = 0; i < 3; i++) {
                        for (int j = -2; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/15;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (y == 1 && x != 0 && x!=1 && x != bufferedImage.getWidth() - 1 &&x!=bufferedImage.getWidth()-2) {
                    for (int i = -1; i < 3; i++) {
                        for (int j = -2; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/20;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (y == bufferedImage.getHeight()-1 && x != 0 && x!=1 && x != bufferedImage.getWidth() - 1 &&x!=bufferedImage.getWidth()-2) {
                    for (int i = -2; i < 1; i++) {
                        for (int j = -2; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/15;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (y == bufferedImage.getHeight()-2 && x != 0 && x!=1 && x != bufferedImage.getWidth() - 1 &&x!=bufferedImage.getWidth()-2) {
                    for (int i = -2; i < 2; i++) {
                        for (int j = -2; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/20;
                    bufferedImage.setRGB(y,x,sum);
                }



                if (x == 0 && y != 0 && y!=1 && y != bufferedImage.getHeight() - 1&& y != bufferedImage.getHeight() - 2) {
                    for (int i = -2; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/15;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (x == 1 && y != 0 && y!=1 && y != bufferedImage.getHeight() - 1&& y != bufferedImage.getHeight() - 2) {
                    for (int i = -2; i < 3; i++) {
                        for (int j = -1; j < 3; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/20;
                    bufferedImage.setRGB(y,x,sum);
                }
//--------------------------------------------------------------------------------------------------
                if (x == bufferedImage.getWidth() - 1 && y != 0 && y!=1&& y != bufferedImage.getHeight() - 1 &&y!=bufferedImage.getHeight()-2) {
                    for (int i = -2; i < 3; i++) {
                        for (int j = -2; j < 1; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/15;
                    bufferedImage.setRGB(y,x,sum);
                }

                if (x == bufferedImage.getWidth() - 2 && y != 0 && y!=1&& y != bufferedImage.getHeight() - 1 &&y!=bufferedImage.getHeight()-2) {
                    for (int i = -2; i < 3; i++) {
                        for (int j = -2; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0;

                        }
                    }
                    sum =sum/20;
                    bufferedImage.setRGB(y,x,sum);
                }
                //-------------------------------------------------------------------------------------------------------------
                if (x != 0 && x != bufferedImage.getWidth() - 1 && y != 0 && y != bufferedImage.getHeight() - 1) {

                    for (int i = -1; i < 2; i++) {
                        for (int j = -1; j < 2; j++) {
                            sum += bufferedImage.getRGB(y + i, x + j) * 0.25;

                        }
                    }
                    sum =sum/9;
                    bufferedImage.setRGB(y,x,sum);
                }
            }


        }

        try {

            ImageIO.write(bufferedImage,"jpg",file);
        }catch (IOException e){
            System.out.println("error:" +e);
        }



    }




}

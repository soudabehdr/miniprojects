package questionThree;

import java.io.*;

/**
 * Created by Soudabeh on 3/18/2017.
 */
public class LCS {


    /**
     * longestCommonSubsequence 2 address be onvane vorodi daryaft mikonad va bozorgtarin zir reshte moshtarak
     * beine in 2 file ra dar yek fiel digar zakhire mikonad
     * @param address1 vorodie aval az noe String ke address file aval k neveshte dar an ast ra midahad
     * @param address2 vorodie dovom az noe String ke address file dovom k neveshte dar an ast ra midahad
     * @throws IOException Exeption az noe IO ke be tabe asli midahim k handle koand
     */
    public void longestCommonSubsequence(String address1 , String address2) throws IOException{

        File file1 = new File(address1); // address aval ra gerefte va file1 migozarim
        File file2 = new File(address2);  // address dovom ra gerefte va file2 migozarim
        File file3= new File("e:\\longest.txt"); // yekk file ijad mikonim baraye neveshtan LCS
        file3.createNewFile();
        FileReader fileReader1= new FileReader(file1);
        BufferedReader bufferedReader1= new BufferedReader(fileReader1);
        FileReader fileReader2=new FileReader(file2);
        BufferedReader bufferedReader2= new BufferedReader(fileReader2);
        String str1=bufferedReader1.readLine();
        String str2=bufferedReader2.readLine();

        int l1 = str1.length();
        int l2 = str2.length();
        int[][] arr = new int[l1 + 1][l2 + 1];

        int len = 0;
        int pos = -1;

        for (int x= 1; x<l1+1 ; x++)
        {
            for (int y= 1; y<l2+1 ; y++)
            {
                if (str1.charAt(x-1) == str2.charAt(y-1))

                {
                    arr[x][y] = arr[x-1][y-1] + 1;

                    if (arr[x][y] > len)

                    {
                        len = arr[x][y];
                        pos = x;
                    }
                }

                else
                    arr[x][y] = 0;

            }

        }

        String result = str1.substring(pos - len, pos);
        FileWriter fileWriter= new FileWriter(file3);
        BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);
        bufferedWriter.write(result);
        System.out.println(result); // in baraye chap kardan injast k nmidonm hadafe sorate soal bood y na
        bufferedWriter.flush();
        bufferedWriter.close();




    }
}


